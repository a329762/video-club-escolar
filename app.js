var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
const mongoose = require('mongoose');

var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
const genresRouter = require('./routes/genres');
const moviesRouter = require('./routes/movies');
const actorsRouter = require('./routes/actors');
const copiesRouter = require('./routes/copies');
const bookingsRouter = require('./routes/bookings');
const membersRouter = require('./routes/members');


// "Mongodb://<dbuser>?:<dbPassword>?@<direction>:<port>/<dbName>"

const uri = "localhost:27017";
mongoose.connect(uri);

const db = mongoose.connection;
const app = express();

db.on('error', ()=>{
  console.log("No se a podido conectar a la db")
});

db.on('open', ()=>{
  
});


// view engine setup => PUG
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
// Middleware Static => Define donde se encuentran todo los recursos estaticos de la app
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/users', usersRouter);
app.use('/', indexRouter);
app.use('/users', usersRouter);
app.use('/genres', genresRouter);
app.use('/movies', moviesRouter);
app.use('/actors', actorsRouter);
app.use('/copies', copiesRouter);
app.use('/bookings', bookingsRouter);
app.use('/members', membersRouter);
app.use('/directors', directorsRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
