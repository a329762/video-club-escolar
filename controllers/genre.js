const express = require('express');
const Genres = require('../models/genre');

// RESTFULL => GET, POST, PUT, PATCH, DELETE 
// Modelo = Una representacion de datos, que representa una entidad del mundo real
function list(req, res, next) {
    Genres.find().then(objs => res.status(200).json({
        message: 'Lista del sistema ',
        obj: objs
    })).catch(ex => res.status(500).json({
        message: 'No se pudo consultar la informacion',
        obj: ex
    }));
}

function index(req, res, next) {
    const id= req.params.id;
    Genres.findOne({"_id":id}).then(obj => res.status(200).json({
        message: 'almacenado con Id ${id]',
        oj: obj
    })).catch(ex => res.status(500).json({
        message: 'No se pudo consultar',
        obj: ex
    }));
}

function create(req, res, next) {
    const status = req.body.status;
    const description = req.body.description;

    let genre = new Genre({
        description:description,
        status:status
    });

    genre.save().then(obj => res.status(200).json({
        message: 'creado correctamente ', 
        obj: obj
    })).catch(ex => res.status(500).json({
        message: 'No se pudo almacenar',
        obj: ex
    }));
}



function replace(req, res, next) {
    const id = req.params.id;
    let status = req.body.status ? req.body.status: "";
    let description = req.body.description ? req.body.description: "";

    let genre = new Object({
        _description:description,
        _status:status
    });

    Genres.findOneAndUpdate({"_id":id}, genre).then(obj => res.status(200).json({
        message: 'reemplazado con Id ${id]',
        oj: obj
    })).catch(ex => res.status(500).json({
        message: 'No se pudo reemplazar',
        obj: ex
    }));
}

function edit(req, res, next) {
    const id = req.params.id;
    const status = req.body.status;
    const description = req.body.description;
    
    let genre = new Object();

    if(status){
        genre._status = status;
    }
    if(description){
        genre._description = description;
    }

    Genres.findOneAndUpdate({"_id":id}, copie).then(obj => res.status(200).json({
        message: 'actualizado con Id ${id]',
        oj: obj
    })).catch(ex => res.status(500).json({
        message: 'No se pudo consultar al el actor',
        obj: ex
    }));
}

function destroy(req, res, next) {
    const id = req.params.id;
    Genres.remove({"_id":id}).then(obj => res.status(200).json({
        message: 'eliminado',
        oj: obj
    })).catch(ex => res.status(500).json({
        message: 'No se pudo eliminar',
        obj: ex
    }));
}

module.exports = {
    list,
    index,
    create,
    replace,
    edit,
    destroy
}