const express = require('express');
const genre = require('../../video-club1/models/genre');
const { Genre } = require('../db');

function list(req, res, next) {
    Genre.findAll({include:['movies']})
        .then(objects => res.json(objects));
}

function index(req, res, next) {
    const id= req.params.id;
    Genre.findByPk(id)
        .then(object => res.json(object));
}

function create(req, res, next) {
    const status = req.body.status;
    const description = req.body.description;

    let genre = new Object({
        description:description,
        status:status
    });
    Genre.create(genre)
        .then(obj => res.json(obj))
        .catch(err => res.send(err));
}

function replace(req, res, next) {
    const id = req.params.id;
    Genre.findByPk(id)
        .then(object => {
            const description = req.body.description ? req.body.description:object.description
            const status = req.body.status ? req.body.status:object.status
            const copiesId = req.body.copiesId ? req.body.copiesId:object.copiesId
            object.update({description:description, status:status})
                .then(genre => res.json(genre))
        })
}

function edit(req, res, next) {
    const id = req.params.id;
    Genre.findByPk(id)
        .then(object => {
            const status = req.body.status ? req.body.status:object.status
            const description = req.body.description ? req.body.description:object.description
            const copiesId = req.body.copiesId ? req.body.copiesId:object.copiesId
            object.update({description:description, status:status})
                .then(genre => res.json(genre))
        })
}

function destroy(req, res, next) {
    const id = req.params.id;
    Genre.destroy({where:{id:id} })
        .then(obj => res.json(obj));
}

module.exports = {
    list,
    index,
    create,
    replace,
    edit,
    destroy
}