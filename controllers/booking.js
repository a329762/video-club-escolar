const express = require('express');
const Booking = require('../models/booking.js');

function list(req, res, next) {
    Booking.find().then(objs => res.status(200).json({
        message: 'Lista de actores del sistema ',
        obj: objs
    })).catch(ex => res.status(500).json({
        message: 'No se pudo consultar la informacion',
        obj: ex
    }));
}

function index(req, res, next) {
    const id = req.params.id;
    Booking.findOne({ "_id": id }).then(obj => res.status(200).json({
        message: 'Actor almacenado con Id ${id]',
        oj: obj
    })).catch(ex => res.status(500).json({
        message: 'No se pudo consultar la el actor',
        obj: ex
    }));
}

function create(req, res, next) {
    const date = req.body.name;
    const copiesId = req.body.copiesId;
    const membersId = req.body.membersId;

    let booking = new Object({
        date: date,
        copiesId: copiesId,
        membersId: membersId,
    });

    booking.save().then(obj => res.status(200).json({
        message: 'Usuario creado correctamente ',
        obj: obj
    })).catch(ex => res.status(500).json({
        message: 'No se pudo almacenar el usuario',
        obj: ex
    }));
}

function replace(req, res, next) {
    const id = req.params.id;
    let date = req.body.date ? req.body.date : "";
    let copiesId = req.body.copiesId ? req.body.copiesId : "";
    let membersId = req.body.membersId ? req.body.membersId : "";

    let booking = new Object({
        _date: date,
        _copiesId: copiesId,
        _membersId: membersId,
    });

    Booking.findOneAndUpdate({ "_id": id }, booking).then(obj => res.status(200).json({
        message: 'Actor reemplazado con Id ${id]',
        oj: obj
    })).catch(ex => res.status(500).json({
        message: 'No se pudo reemplazar al el actor',
        obj: ex
    }));
}

function edit(req, res, next) {
    const id = req.params.id;
    let date = req.body.date;
    let copiesId = req.body.copiesId;
    let membersId = req.body.membersId;

    let booking = new Object();

    if (date) {
        booking._date = date;
    }
    if (copiesId) {
        booking._copiesId = copiesId;
    }
    if (membersId) {
        booking._membersId = membersId;
    }

    Booking.findOneAndUpdate({ "_id": id }, acto).then(obj => res.status(200).json({
        message: 'Actor actualizado con Id ${id]',
        oj: obj
    })).catch(ex => res.status(500).json({
        message: 'No se pudo consultar al el actor',
        obj: ex
    }));
}

function destroy(req, res, next) {
    const id = req.params.id;
    Booking.remove({"_id":id}).then(obj => res.status(200).json({
        message: 'Actor eliminado',
        oj: obj
    })).catch(ex => res.status(500).json({
        message: 'No se pudo eliminar al el actor',
        obj: ex
    }));
}

module.exports = {
    list,
    index,
    create,
    replace,
    edit,
    destroy
}