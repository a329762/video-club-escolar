const express = require('express');
const Member = require('../models/member');

// RESTFULL => GET, POST, PUT, PATCH, DELETE 
// Modelo = Una representacion de datos, que representa una entidad del mundo real
function list(req, res, next) {
    Member.find().then(objs => res.status(200).json({
        message: 'Lista del sistema ',
        obj: objs
    })).catch(ex => res.status(500).json({
        message: 'No se pudo consultar la informacion',
        obj: ex
    }));
}

function index(req, res, next) {
    const id= req.params.id;
    Member.findOne({"_id":id}).then(obj => res.status(200).json({
        message: 'almacenado con Id ${id]',
        oj: obj
    })).catch(ex => res.status(500).json({
        message: 'No se pudo consultar',
        obj: ex
    }));
}

function create(req, res, next) {
    const name = req.body.name;
    const lastName = req.body.lastName;
    const status = req.body.status;
    const address = req.body.address;
    const phone = req.body.phone;

    let member = new Member({
        name:name,
        lastName:lastName,
        status:status,
        address:address,
        phone:phone
    });

    member.save().then(obj => res.status(200).json({
        message: 'creado correctamente ', 
        obj: obj
    })).catch(ex => res.status(500).json({
        message: 'No se pudo almacenar',
        obj: ex
    }));
}



function replace(req, res, next) {
    const id = req.params.id;
    let name = req.body.name ? req.body.name: "";
    let lastName = req.body.lastName ? req.body.lastName: "";
    let address = req.body.address ? req.body.formaaddresst: "";
    let phone = req.body.phone ? req.body.phone: "";
    let status = req.body.status ? req.body.status: True;

    let member = new Object({
        _name:name,
        _lastName:lastName,
        _status:status,
        _address:address,
        _phone:phone
    });

    Member.findOneAndUpdate({"_id":id}, member).then(obj => res.status(200).json({
        message: 'reemplazado con Id ${id]',
        oj: obj
    })).catch(ex => res.status(500).json({
        message: 'No se pudo reemplazar',
        obj: ex
    }));
}

function edit(req, res, next) {
    const name = req.body.name;
    const lastName = req.body.lastName;
    const status = req.body.status;
    const address = req.body.address;
    const phone = req.body.phone;
    
    let member = new Object();

    if(name){
        member._name = name;
    }
    if(lastName){
        member._lastName = lastName;
    }
    if(status){
        member._status = status;
    }
    if(address){
        member._address = address;
    }
    if(address){
        member._phone= phone;
    }

    Member.findOneAndUpdate({"_id":id}, member).then(obj => res.status(200).json({
        message: 'actualizado con Id ${id]',
        oj: obj
    })).catch(ex => res.status(500).json({
        message: 'No se pudo consultar al el actor',
        obj: ex
    }));
}

function destroy(req, res, next) {
    const id = req.params.id;
    Member.remove({"_id":id}).then(obj => res.status(200).json({
        message: 'eliminado',
        oj: obj
    })).catch(ex => res.status(500).json({
        message: 'No se pudo eliminar',
        obj: ex
    }));
}

module.exports = {
    list,
    index,
    create,
    replace,
    edit,
    destroy
}