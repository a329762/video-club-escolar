const express = require('express');
const Users = require('../models/users');

// RESTFULL => GET, POST, PUT, PATCH, DELETE 
// Modelo = Una representacion de datos, que representa una entidad del mundo real
function list(req, res, next) {
    Users.find().then(objs => res.status(200).json({
        message: 'Lista de actores del sistema ',
        obj: objs
    })).catch(ex => res.status(500).json({
        message: 'No se pudo consultar la informacion',
        obj: ex
    }));
}

function index(req, res, next) {
    const id= req.params.id;
    Users.findOne({"_id":id}).then(obj => res.status(200).json({
        message: 'Actor almacenado con Id ${id]',
        oj: obj
    })).catch(ex => res.status(500).json({
        message: 'No se pudo consultar la el actor',
        obj: ex
    }));
}

function create(req, res, next) {
    const name = req.body.name;
    const lastName = req.body.lastName;
    const email = req.body.email;
    const password = req.body.password;

    let users = new Users({
        name:name,
        lastName:lastName,
        email:email,
        password:password
    });

    users.save().then(obj => res.status(200).json({
        message: 'Usuario creado correctamente ', 
        obj: obj
    })).catch(ex => res.status(500).json({
        message: 'No se pudo almacenar el usuario',
        obj: ex
    }));
}



function replace(req, res, next) {
    const id = req.params.id;
    let name = req.body.name ? req.body.name: "";
    let lastName = req.body.lastName ? req.body.lastName: "";
    let email = req.body.email ? req.body.email: "";
    let password = req.body.password ? req.body.password: "";

    let users = new Object({
        _name:name,
        _lastName:lastName,
        _email:email,
        _password:password
    });

    Users.findOneAndUpdate({"_id":id}, users).then(obj => res.status(200).json({
        message: 'Actor reemplazado con Id ${id]',
        oj: obj
    })).catch(ex => res.status(500).json({
        message: 'No se pudo reemplazar al el actor',
        obj: ex
    }));
}

function edit(req, res, next) {
    const name = req.body.name;
    const lastName = req.body.lastName;
    const email = req.body.email;
    const password = req.body.password;
    
    let users = new Object();

    if(name){
        users._name = name;
    }
    if(lastName){
        users._lastName = lastName;
    }
    if(email){
        users._email = email;
    }
    if(password){
        users._password = password;
    }

    Users.findOneAndUpdate({"_id":id}, users).then(obj => res.status(200).json({
        message: 'Actor actualizado con Id ${id]',
        oj: obj
    })).catch(ex => res.status(500).json({
        message: 'No se pudo consultar al el actor',
        obj: ex
    }));
}

function destroy(req, res, next) {
    const id = req.params.id;
    Users.remove({"_id":id}).then(obj => res.status(200).json({
        message: 'Actor eliminado',
        oj: obj
    })).catch(ex => res.status(500).json({
        message: 'No se pudo eliminar al el actor',
        obj: ex
    }));
}

module.exports = {
    list,
    index,
    create,
    replace,
    edit,
    destroy
}