const express = require('express');
const { Movie, Actor } = require('../db');

function list(req, res, next) {
    Movie.findAll({include:['members', 'copies']})
        .then(objects => res.json(objects));
}

function index(req, res, next) {
    const id= req.params.id;
    Movie.findByPk(id)
        .then(object => res.json(object));
}

function create(req, res, next) {
    const tittle = req.body.tittle;
    const genreId = req.body.genreId;

    let movie = new Object({
        tittle:tittle,
        genreId:genreId,
    });
    Movie.create(movie)
        .then(obj => res.json(obj))
        .catch(err => res.send(err));
}

function replace(req, res, next) {
    const id = req.params.id;
    Movie.findByPk(id)
        .then(object => {
            const tittle = req.body.tittle ? req.body.tittle:object.tittle
            const genreId = req.body.genreId ? req.body.genreId:object.genreId
            object.update({date:date, membersId:membersId, copiesId:copiesId})
                .then(movie => res.json(movie))
        })
}

function edit(req, res, next) {
    const id = req.params.id;
    Movie.findByPk(id)
        .then(object => {
            const tittle = req.body.tittle ? req.body.tittle:object.tittle
            const genreId = req.body.genreId ? req.body.genreId:object.genreId
            object.update({date:date, membersId:membersId, copiesId:copiesId})
                .then(movie => res.json(movie))
        })
}

function destroy(req, res, next) {
    const id = req.params.id;
    Movie.destroy({where:{id:id} })
        .then(obj => res.json(obj));
}

module.exports = {
    list,
    index,
    create,
    replace,
    edit,
    destroy
}