const express = require('express');
const Movie = require('../models/movie');

// RESTFULL => GET, POST, PUT, PATCH, DELETE 
// Modelo = Una representacion de datos, que representa una entidad del mundo real
function list(req, res, next) {
    Movie.find().then(objs => res.status(200).json({
        message: 'Lista del sistema ',
        obj: objs
    })).catch(ex => res.status(500).json({
        message: 'No se pudo consultar la informacion',
        obj: ex
    }));
}

function index(req, res, next) {
    const id= req.params.id;
    Movie.findOne({"_id":id}).then(obj => res.status(200).json({
        message: 'almacenado con Id ${id]',
        oj: obj
    })).catch(ex => res.status(500).json({
        message: 'No se pudo consultar',
        obj: ex
    }));
}

function create(req, res, next) {
    const tittle = req.body.tittle;

    let movie = new Movie({
        tittle:tittle,
    });

    movie.save().then(obj => res.status(200).json({
        message: 'creado correctamente ', 
        obj: obj
    })).catch(ex => res.status(500).json({
        message: 'No se pudo almacenar',
        obj: ex
    }));
}



function replace(req, res, next) {
    const id = req.params.id;
    let tittle = req.body.tittle ? req.body.tittle: "";

    let movie = new Object({
        _tittle:tittle,
    });

    Movie.findOneAndUpdate({"_id":id}, movie).then(obj => res.status(200).json({
        message: 'reemplazado con Id ${id]',
        oj: obj
    })).catch(ex => res.status(500).json({
        message: 'No se pudo reemplazar',
        obj: ex
    }));
}

function edit(req, res, next) {
    const tittle = req.body.tittle;
    
    let movie = new Object();

    if(tittle){
        movie._tittle = tittle;
    }

    Movie.findOneAndUpdate({"_id":id}, movie).then(obj => res.status(200).json({
        message: 'actualizado con Id ${id]',
        oj: obj
    })).catch(ex => res.status(500).json({
        message: 'No se pudo consultar al el actor',
        obj: ex
    }));
}

function destroy(req, res, next) {
    const id = req.params.id;
    Movie.remove({"_id":id}).then(obj => res.status(200).json({
        message: 'eliminado',
        oj: obj
    })).catch(ex => res.status(500).json({
        message: 'No se pudo eliminar',
        obj: ex
    }));
}

module.exports = {
    list,
    index,
    create,
    replace,
    edit,
    destroy
}