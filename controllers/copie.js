const express = require('express');
const Copies = require('../models/copie');

// RESTFULL => GET, POST, PUT, PATCH, DELETE 
// Modelo = Una representacion de datos, que representa una entidad del mundo real
function list(req, res, next) {
    Copies.find().then(objs => res.status(200).json({
        message: 'Lista del sistema ',
        obj: objs
    })).catch(ex => res.status(500).json({
        message: 'No se pudo consultar la informacion',
        obj: ex
    }));
}

function index(req, res, next) {
    const id= req.params.id;
    Copies.findOne({"_id":id}).then(obj => res.status(200).json({
        message: 'almacenado con Id ${id]',
        oj: obj
    })).catch(ex => res.status(500).json({
        message: 'No se pudo consultar',
        obj: ex
    }));
}

function create(req, res, next) {
    const number = req.body.number;
    const estatus = req.body.estatus;
    const format = req.body.format;
    const moviesId = req.body.moviesId;

    let copie = new Copie({
        number:number,
        estatus:estatus,
        format:format,
        moviesId:moviesId
    });

    copie.save().then(obj => res.status(200).json({
        message: 'creado correctamente ', 
        obj: obj
    })).catch(ex => res.status(500).json({
        message: 'No se pudo almacenar',
        obj: ex
    }));
}



function replace(req, res, next) {
    const id = req.params.id;
    let number = req.body.number ? req.body.number: "";
    let estatus = req.body.estatus ? req.body.estatus: "";
    let format = req.body.format ? req.body.format: "";
    let moviesId = req.body.moviesId ? req.body.moviesId: "";

    let copie = new Object({
        _number:number,
        _estatus:estatus,
        _format:format,
        _moviesId:moviesId
    });

    Copie.findOneAndUpdate({"_id":id}, copie).then(obj => res.status(200).json({
        message: 'reemplazado con Id ${id]',
        oj: obj
    })).catch(ex => res.status(500).json({
        message: 'No se pudo reemplazar',
        obj: ex
    }));
}

function edit(req, res, next) {
    const id = req.params.id;
    const number = req.body.number;
    const estatus = req.body.estatus;
    const format = req.body.format;
    const moviesId = req.body.moviesId;
    
    let copie = new Object();

    if(number){
        copie._number = number;
    }
    if(estatus){
        copie._estatus = estatus;
    }
    if(format){
        copie._format = format;
    }
    if(moviesId){
        copie._moviesId = moviesId;
    }

    Copies.findOneAndUpdate({"_id":id}, copie).then(obj => res.status(200).json({
        message: 'actualizado con Id ${id]',
        oj: obj
    })).catch(ex => res.status(500).json({
        message: 'No se pudo consultar al el actor',
        obj: ex
    }));
}

function destroy(req, res, next) {
    const id = req.params.id;
    Copies.remove({"_id":id}).then(obj => res.status(200).json({
        message: 'eliminado',
        oj: obj
    })).catch(ex => res.status(500).json({
        message: 'No se pudo eliminar',
        obj: ex
    }));
}

module.exports = {
    list,
    index,
    create,
    replace,
    edit,
    destroy
}