const mongoose = require('mongoose');

const schema = mongoose.Schema({
    _movieId:Integer,
    _actorId:Integer
});

class MovieActor {
    constructor(movieId, actorId){
        this._movieId=movieId;
        this._actorId=actorId;
    }

    get movieId(){
        return this._movieId;
    }
    set movieId(v){
        this._movieId=v;
    }
    get actorId(){
        return this._actorId;
    }
    set actorId(v){
        this._actorId=v;
    }

}

schema.loadClass(MovieActor);
module.exports = mongoose.model('MovieActor', schema);