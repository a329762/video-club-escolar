const mongoose = require('mongoose');

const schema = mongoose.Schema({
    _number:String,
    _format:String,
    _estatus:String
});

class Copie {
    constructor(number, lastName, estatus){
        this._number=number;
        this._format=format;
        this._estatus=estatus;
    }

    get number(){
        return this._number;
    }
    set number(v){
        this._number=v;
    }
    get format(){
        return this._format;
    }
    set format(v){
        this._format=v;
    }
    get estatus(){
        return this._estatus;
    }
    set estatus(v){
        this._estatus=v;
    }
}

schema.loadClass(Copie);
module.exports = mongoose.model('Copie', schema);