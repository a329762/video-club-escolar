module.exports = (sequelize, type) => {
    const Copies = sequelize.define('copies', {
        id:{type:type.INTEGER, primaryKey:true, autoIncrement:true},
        title:{type:type.STRING, notNull:true}
    });
    return Copies;
};