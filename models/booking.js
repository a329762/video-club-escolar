const mongoose = require('mongoose');

const schema = mongoose.Schema({
    _date:String,
    _membersId:String,
    _copiesId:String
});

class Booking {
    constructor(date, membersId, copiesId){
        this._date=date;
        this._membersId=membersId;
        this._copiesId=copiesId;
    }

    get date(){
        return this._date;
    }
    set date(v){
        this._date=v;
    }
    get membersId(){
        return this._membersId;
    }
    set membersId(v){
        this._membersId=v;
    }
    get copiesId(){
        return this._copiesId;
    }
    set copiesId(v){
        this._copiesId=v;
    }
}

schema.loadClass(Booking);
module.exports = mongoose.model('Booking', schema);