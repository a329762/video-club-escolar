const mongoose = require('mongoose');

const schema = mongoose.Schema({
    _title:String,
});

class Movie {
    constructor(title){
        this._title=title;
    }

    get title(){
        return this._title;
    }
    set title(v){
        this._title=v;
    }
}

schema.loadClass(Movie);
module.exports = mongoose.model('Copies', schema);