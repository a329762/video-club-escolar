module.exports = (sequelize, type) => {
    const Copies = sequelize.define('copies', {
        id:{type:type.INTEGER, primaryKey:true, autoIncrement:true},
        number:type.STRING,
        format:type.STRING,
        estatus:type.STRING
    });
    return Copies;
};