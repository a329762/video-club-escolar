const Sequelize = require('sequelize');
const directorModel = require('./models/director');
const genreModel = require('./models/genre');
const movieModel = require('./models/movie');
const actorModel = require('./models/actor');
const movieActorModel = require('./models/movieActor');
const Copies = require('./models/copies');
const bookingModel = require('./models/booking');
const memberModel = require('./models/member');

const sequelize = new Sequelize('video-club', 'root', 'rt', {
    host:'localhost',
    dialect:'mysql'
});

const Director = directorModel(sequelize, Sequelize);
const Genre = genreModel(sequelize, Sequelize);
const Movie = movieModel(sequelize, Sequelize);
const Actor = actorModel(sequelize, Sequelize);
const MovieActor = movieActorModel(sequelize, Sequelize);
const Copies = copiesModel(sequelize, Sequelize);
const Booking = bookingModel(sequelize, Sequelize);
const Member = memberModel(sequelize, Sequelize);

Genre.hasMany(Movie, {as:'movies'});

Movie.belongsTo(Genre, {as:'genre'});

Director.hasMany(Movie, {as:'movies'});

Movie.belongsTo(Director, {as:'director'});

MovieActor.belongsTo(Movie, {foreignKey:'movieId'});

MovieActor.belongsTo(Actor, {foreignKey:'actorId'});

Movie.belongsToMany(Actor, {
    foreignKey: 'actorId',
    as: 'actors',
    through: 'moviesActors'
})
Actor.belongsToMany(Movie, {
    foreignKey: 'movieId',
    as: 'movies',
    through: 'moviesActors'
})

Movie.hasMany(Copies, {as:'copies'});

Copies.belongsTo(Movie, {as:'movies'});

Copies.hasMany(Booking, {as:'bookings'});

Booking.belongsTo(Copies, {as:'copies'});

Member.hasMany(Booking, {as:'bookings'});

Booking.belongsTo(Member, {as:'members'});

sequelize.sync({
    force:true
}).then(()=>{
    console.log("base actualizada");
});

module.exports = {Director, Genre, Movie, Actor, MovieActor, Copies, Booking, Member}